import os

shopping_list = []
items = []

def define_shopping_list():
    list_name = input("Enter a name for your shopping list ")
    shopping_list.append(list_name)
    return list_name

def add_items():
    add_item = input("item that you want to add ")
    items.append(add_item)
    return add_item

def clear_screen():
    os.system("cls")

def show_list():
    clear_screen()
    print("items for {} shopping list are: \n".format("".join(shopping_list)))

    index = 1
    for i in items:
        print("{}. {} ".format(index, i)+'\n')
        index+=1


#def add_new_items(new_item):
 #   items.insert(-1, new_item)


def logic():
    list_name = define_shopping_list()

    print("shopping list '{}' has been created successfully ".format(list_name))
    start_list = input("Do you want to start adding items to your list? 'y' for yes ")
    while start_list == 'y':
        updated_items = add_items()
        start_list = input("want to add more ")

    show_list()

  #  add_values_list = input("Do you want to add more items? ")

  #  while add_values_list.lower() == 'y':
  #      new_updated_items = add_items()
   #     add_new_items(new_updated_items)
    #    add_values_list = input("want to add more ")

    #show_list()
    print("\n Thank you for shopping")


logic()
